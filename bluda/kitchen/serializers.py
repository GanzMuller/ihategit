from rest_framework import serializers

from .models import *

class BludoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Bludo
        fields = ('id', 'name', 'describe', 'groupof', 'ingridients')

class IngridientsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Ingridients
        fields = ('id', 'name', 'supplier', 'date_arrive')

class PovarSerializer(serializers.ModelSerializer):
    class Meta:
        model = Povar
        fields = ('id', 'fio', 'master', 'date_birth', 'bludo')