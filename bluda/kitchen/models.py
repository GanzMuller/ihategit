from django.db import models
from django.utils import timezone

# Create your models here.

class Ingridients(models.Model):
    name = models.CharField(
        verbose_name='Название ингридиента',
        null=False,
        max_length=20,
        help_text='Пример: "Титан"'
    )

    supplier = models.CharField(
        verbose_name='Поставщик',
        help_text='Пример: "ООО Братишка"',
        max_length=100,
        null=False,  
    )
    date_arrive = models.DateTimeField(
        verbose_name='Дата поставки',
        default=timezone.now,
        null=False,
    )
    
    def __str__(self):
        return f"{self.name} {self.supplier}"

class Bludo(models.Model):
    groups = [
        ('5', 'Добавка'),
        ('4', 'Напиток'),
        ('3', 'Десерт'),
        ('2', 'Второе'),
        ('1', 'Первое'),
    ]

    name = models.CharField(
        verbose_name='Название блюда',
        max_length=50,
        help_text='Пример: "Вареный кирпич"',
        null=False,
    )

    describe = models.TextField(
        verbose_name='Описание',
        max_length=350,
        help_text='Пример: "Готовит однажды сталкер..."'
    )

    groupof = models.CharField(
        verbose_name='Группа блюда',
        choices=groups,
        default='1',
        null=False,
        max_length=1
    )

    ingridients = models.ManyToManyField(
        Ingridients,
        verbose_name='Ингридиенты',
        blank=False,
    )

    def __str__(self):
        return self.name
        
class Povar(models.Model):
    charity = [
        ('SSS', 'Невозможный'),
        ('SS', 'Крутой'),
        ('S', 'Отличный'),
        ('A', 'Обычный'),
        ('B', 'Непримечательный')
    ]

    fio = models.CharField(
        verbose_name='ФИО',
        max_length=100,
        null=False,
        help_text='Пример: "Пантелеймонов Пантелей Пантелеевич"'
    )

    master = models.CharField(
        verbose_name='Мастерство',
        choices=charity,
        default='SSS',
        null=False,
        max_length=3
    )
    
    date_birth = models.DateField(
        verbose_name='Дата рождения',
        default=timezone.now
    )

    bludo = models.ManyToManyField(
        Bludo,
        verbose_name='Что готовит',
        blank=True
    )   