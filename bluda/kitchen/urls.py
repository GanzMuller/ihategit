from django.urls import path
from rest_framework.routers import DefaultRouter
from .views import *



router = DefaultRouter()
router.register(r'bludo', BludoViewSet, basename='bludo')
router.register(r'povar', PovarViewSet, basename='povar')
router.register(r'ingridient', IngridientViewSet, basename='ingridient')

urlpatterns = router.urls