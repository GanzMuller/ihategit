from django.shortcuts import get_object_or_404, render
from rest_framework import viewsets
from rest_framework.response import Response
from .models import *
from .serializers import *
from rest_framework.permissions import IsAuthenticated 

# Create your views here.



class BludoViewSet(viewsets.ModelViewSet):
    # permission_classes = (IsAuthenticated, )
    serializer_class = BludoSerializer
    queryset = Bludo.objects.all()

class IngridientViewSet(viewsets.ModelViewSet):
    # permission_classes = (IsAuthenticated, )
    serializer_class = IngridientsSerializer
    queryset = Ingridients.objects.all()

class PovarViewSet(viewsets.ModelViewSet):
    # permission_classes = (IsAuthenticated, )
    serializer_class = PovarSerializer
    queryset = Povar.objects.all()